# What to do Now
- Change mass at some point
- Change potential at some spring
 - Double well?
 - non-linear single well potential? Like the strange chain in the article nmat 3072
 - etc...
- Change friction at some point
 - Dry friction
 - Strong damping at some point? Seems to be not applicable
- Find the threshold value according analytical work

# Long Chain?
Pros

+ For longer chain, their may be more effective difference in amplitude at the beginning of the special spring. 
+ The band width will be slightly wider. However, the band width will be no larger than $2\omega_0$. For the degree of freedom in the range of 1-10, the maximum of eigen values is::

    1 1.0
    2 1.61803398875
    3 1.8019377358
    4 1.87938524157
    5 1.91898594723
    6 1.94188363485
    7 1.95629520147
    8 1.96594619937
    9 1.97272260681
    10 1.97766165245


+ The distribution of eigen values will be more dense so the resonance will be significantly homogenized. Thus the resonance at the critical point will be larger. As a consequence, the proper friction to make the resonance smooth and strong decrease faster than $O(1/n)$. It is also observed in the simulation.
+ As you stated, If the chain is long, there will be no need for a large viscosity. But the longer the chain is, the more difficult for us to implement. So it depends on the actual facilities to find a compromise.
+ Even though the band is wider, the amplitude will definitely decrease quickly like what the cosine curve do near $\pi/2$ when the frequency increase 
+ How to implement the solid friction? Maybe use the digital circuit like system?

+ The longer the chain is, the more difficult for us to implement the model
+ What is the critical friction?
 - Since the potential Matrix is similar to the fixed condition, there are some reasons to suppose that it is similar to the free boundary condition

# Step $n+1$
+ Calculate the critical friction
+ Analysis the output spectrum with fourier transformation to see how much effective information we did get.

## Critical friction
+ The best place to set dry friction is the 2nd final particle. If we set the friction to be a critical one, and the chain is long, the peak value of fixed condition seems to be in the center---$\omega_0$. We must set the friction to be at least larger than that. So, if the peak is too large, 

## Zeb's Suggestions
+ draw a contour plot?
 - Range of frequency be $arange(0.05, 2.2, 0.05)$
 - Amplitude of drive be $arange(0.4, 3.2, 0.1)$
+ Calculate the action of double well?
 - Calculate the kinetic energy of Each ball as a function of time
 - Calculate the potential of each spring as a function of time
+ Calculate transmission rate? How to?


    w1 w2 w3 w4....
a1A
a1B
a2A
a2B
...
