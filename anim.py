#!/usr/bin/env python3
#Show animation of a specific point
from numpy import *
from matplotlib.pyplot import *
from matplotlib.animation import FuncAnimation
from simulate import *
def yie():
    i=0
    while i<=200:
        yield cos(i),sin(i)
        i+=2*pi/60

fg,ax=subplots()
ln,=ax.plot([],[],'o-',lw=2)
#axis('equal')
grid()
ll,rr=5,5
sig=0
if sig:
    ll,rr=[rr,ll]
aww=[2,0.1]
t=0
act=array(vibrate(t,[ll,rr],r=0.25,aw=aww,spec='fix')[1])
def run(i):
    xd=range(ll+rr)
    yd=act[i]
    ln.set_data(xd,yd)
    return ln,

l=len(act)
if 1:
    ax.set_xlim(-1,ll+rr)
    ax.set_ylim(-aww[0]-1,aww[0]+1)
    ani = FuncAnimation(fg, run, range(l), blit=False, interval=20, repeat=False)
else:
    plot(act[:,-1],'-')
show()
