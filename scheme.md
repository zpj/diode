# Simulate Motion of Balls connected by springs
O-----O-----O-----O=====O-----O

## Potential
+ ----- represent a linear spring whose potential is $V_1=kdx^2/2$
+ ===== represent a non-linear spring whose potential is $V_2=k'(x^2-c^2)^2/4$
+ friction $f=-\gamma dv$
+ etc.

## Details for Calculation
+ Euler's Method
+ Parameters
 - Balls in the left/right, $n_l, n_r$
 - Stiffness $k$
 - Stiffness $k'$ along with mininum position $\pm c$
+ What to save?
 - Position and Speed
 - Set the oscillation in one end and the other without outer force
+ What to get?
 - The amplitude of the other end
 - Show it with animation?

# Technological details
## Data Structure, what to save?
+ Save spatial deviation of equilibrium in an array `x`
+ Save the velocity in an array `v`
+ Calculate the Force as a function of `x` and `v`, then get the accelerate
+ if the mass was not equivalent for all objects, there should be a mass array

## Psudocode description
+ set initial value x
+ start a loop
 - set boundary condition
 - calculate the devition of other balls in the next moment $x'=x+v\Delta t$
 - $v'=v+a\Delta t$
 - $F'=F(x',v')$
 - $a'=\dfrac{F'}{m}$
+ after enough loops, calculate the amplitude of the ball in the other end
+ Find an typical value make it easy to overcome the double-well potential in one end but hard in the other end

## How large should the friction be?
- For $4$ springs, $\gamma=0.5-0.65$ is a good choice.
- For $10$ springs, $\gamma=0.23-0.32$
- $4$ Springs, eigenvals is about $0.45,1.25,1.79$
 
## max eigen value
[1.0,
 1.6180339887498949,
 1.8019377358048381,
 1.8793852415718166,
 1.9189859472289947,
 1.9418836348521038,
 1.9562952014676114,
 1.9659461993678033,
 1.9727226068054449]
