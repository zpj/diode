#!/usr/bin/env python3
## Calculate the amplitude
from numpy import *
from matplotlib.pyplot import *
from matplotlib.animation import FuncAnimation
set_printoptions(formatter={'float': '{: 0.3f}'.format})
DEBUG=False
CMD=True
MANUAL=False

def kinetic(v):
    '''$E_k=mv**2/2$'''
    return 0.5*mean(v*v)

#def rng(lst):
    #'''range of a list'''
    #return max(lst)-min(lst)

def deviation(lst):
    '''relative deviation between max and min'''
    return (max(lst)-min(lst))/min(lst)

#def fluct(a):
    #'''Judge if it is still vibrating or being stable'''
    #return min(a[-5:])/max(a[-5:])<0.8

#def isPrdn(lst,n=12,th=0.03):
    #'''
    #Judge the validty of n as a period
    #'''
    #half=max([n//2,5])
    #l=len(lst)
    #ave=zeros(half)
    #for i in range(half):
        #ave[i]=mean(lst[-n-i:l-i])
    #k=rng(ave)/rng(lst[-half-n:])*n
    #if k<th:
        #if DEBUG:
            #print(ave, k)
        #return ave[-1]
    #else:
        #return False

#def getPrd(lst,detail=False):
    #'''
    #Try to get the period of a list
    #'''
    #pmarr=range(2,len(lst)//2)
    #for i in pmarr:
        #p=isPrdn(lst,i)
        #if p:
            #return i,p
    #return -1,0


def vibStep(inter, frct, drv, dt, sample, stoptime=inf, mass=1):
    '''To write a function yield the steps of a dynamic chain system
    which only has nearest interaction and a friction as a function of velocity.
    
    Initial state be static in terms of position and velocity.
    
    Args:
        inter:  Interaction potential list. 
        frct:   Friction function list. 0 mean friction and other positive real number mean stiffness of particles(1 by default).
        drv:    Drive function that give the position of the 1st particle
        dt:     Grain of time
        sample: How long to sample, must be integer
        stoptime:   Time to stop, default to be infinite
        mass:   Mass of particles
    '''
    if isinstance(mass, int):
        n=len(frct)
    else:
        n=len(m)
    if not (len(inter)+1 is len(frct) is n):
        print('Length not match!')
        return
    x=zeros(n)
    v=zeros(n)
    F=zeros(n)
    a=zeros(n)
    intF=zeros(n-1)
    vecfun = vectorize(lambda f, x: f(x))
    yield x,v
    while n*dt<stoptime:
        v=v+a*dt
        v[0]=(drv((n+0.5)*dt)-drv((n-0.5)*dt))/dt
        x=x+v*dt
        x[0]=drv(n*dt)
        y=diff(x)
        intF=vecfun(inter,y)
        F=vecfun(frct,v)
        F[1:]+=intF
        F[:-1]-=intF
        a=F/mass
        a[0]=0
        n+=1
        if n%sample ==0:
            yield x,v

def vibWrap(numB, indS, r, amp, omega, stf, dt, sample, stoptime):
    '''Wrap the parameters according to different vibration types,
    friction, drive, stiffness etc. and give a generator
    The generator is useful for later calculation of the energy
    Args:
        numB:   Number of balls of sum/index
        indS:   Index of special point
        r:      Friction Coeffecient
        amp:    Amplitude of drive
        omega:  Angular frequency. Not circular frequency!
        stf:    Stiffness of spring
        dt:     minimal step of calculation
        sample: Interval of sampling
        stoptime:   Time to stop
        ftype   is the friction dry or damping?Z
    '''
    sinD=lambda t:-amp*sin(omega*t)    #sinusoidal driving
    dmpF=lambda v:-r*v        #damping friction
    dryF=lambda v:-r*sign(v)  #static/dry friction
    harP=lambda x:0.5*stf*x*x       #harmonic spring
    harS=lambda x:-stf*x       #harmonic spring
    inter=[harS for i in range(numB-1)]
    pot=[harP for i in range(numB-1)]
    frct=[dmpF for i in range(numB)]
    drv=sinD
    T=2*pi/omega
    #```
    #elif vibTyp == 2:
        #inter[indS]=dwS
        #pot[indS]=dwP
    #```
    stp=vibStep(inter, frct, drv, dt, sample, stoptime)
    return stp, pot

def unstableE(lst,man=False):
    '''
    Calculate the energy even if it is not stable
    '''
    if deviation(lst)>0.25:
        if MANUAL and man:
            plot(lst,'o-')
            xlim(0,len(lst)+1)
            show()
            s=input('Input the estimation: ')
            return eval(s)
        return mean(lst[-5:])
    else:
        if DEBUG:
            print('> DEBUG: Temporarily not converge!', lst[-10:])
        return mean(lst[-5:])
    
def isStable(lst, th=0.01):
    '''Return:
        0: Zero
        1: Normal
        -1: Unstable'''
    tail=5
    perS=8
    if len(lst)>tail:
        if deviation(lst[-tail:]) < th:
            return 1, lst[-1]
        else:
            return -1, unstableE(lst)

def contrt(lst,n):
    '''Calculate the mean energy of all periods. Period length is n'''
    l=len(lst)
    shift=l%n
    c=zeros(l//n)
    for i in range(l//n):
        c[i]=mean(lst[n*i+shift:n*(i+1)+shift])
    return c

def printStatus(status, energy):
    print('>'*5,'Status = %d,\tEnergy = %.6f'%(status, energy))
    return

def vibErg(numB=4, indS=-1, r=1, amp=1, omega=1, stf=1, mode='T', sample=50, period=2000, stoptime=20, vibTyp=1, cap=9):
    '''
    Args:
        mode:   T   Calculate the energy of tail particle only
                E   Calculate the energy of all particles
                x   Show the x of all particles
                v   Show the v of all particles
                d   blah
    '''
    T=2*pi/omega
    dt=T/period
    mass=1
    stoptime=stoptime*numB*T# Stop time
    stp, potf=vibWrap(numB, indS, r, amp, omega, vibTyp, stf, dt, sample, stoptime)
    xlst=[]
    vlst=[]
    dlst=[]
    Ekl=[]  #E_k of the last particle
    if period%sample:
        print('Error: sample should be a divisor of time slices')
        return
    numinperiod=period//sample# number of samples in a period
    vcache=zeros(numinperiod)
    
    # Loop until Stable
    for x,v in stp:
        xlst.append(x)
        dlst.append(diff(x))
        vlst.append(v)
        vcache[len(vlst)%numinperiod]=v[-1]
        if len(vlst)%numinperiod == 0:
            Ekl.append(kinetic(vcache))
            if len(Ekl)<6:
                continue
            a=isStable(Ekl)
            if a[0] > -1:
                if mode is 'T':
                    if CMD:
                        printStatus(*a)
                    if DEBUG:
                        if a[0]>1:
                            clf()
                            plot(Ekl,'o-')
                            title(a[0])
                            show()
                        elif a[0]==1 and len(Ekl)>15:
                            clf()
                            plot(Ekl,'o-')
                            title(a[0])
                            show()
                    return a[1]
                else:
                    break
    else:
        if mode is 'T':
            a=isStable(Ekl)
            #if CMD:
                #printStatus(*a)
            if a[0]==-1:
                unstableE(Ekl, True)
            return a[1]
    # Now the state is finally stable
    
    # Following is the data visualization
    vlst=array(vlst).T
    xlst=array(xlst).T
    dlst=array(dlst).T
    pot=array([isStable(contrt(potf[i](d),numinperiod))[1] for i,d in enumerate(dlst)])
    knt=array([isStable(contrt(i*i,numinperiod))[1] for i in vlst])*0.5
    if mode is 'E':
        plot(arange(len(pot))+0.5,pot,'o-')
        plot(knt,'o-')
        #if CMD:
            #print(knt)
            #print(pot)
        show()
        return pot,knt
    else:
        if mode is 'v':
            toshow=vlst[-cap:]
        elif mode is 'x':
            toshow=xlst[-cap:]
        elif mode is 'd':
            toshow=dlst
        for i,j in enumerate(toshow):
            plot(j[-(2+1.5*a[0])*numinperiod:],label=str(i))
        legend()
        title(mode)
        show()
        return a[1]

if __name__=='__main__':
    olst=[0.6]
    alst=[1,1.2,1.4,1.7,2.0,2.4,2.5]
    #olst=[0.7,0.78,0.79,0.80]
    h=zeros_like(olst)
    g=zeros_like(olst)
    l=zeros_like(olst)
    xlim(min(olst),max(olst)+0.3*(max(olst)-min(olst)))
    A=2.45
    S=A**2
    for i,w in enumerate(olst):
        for A in alst:
            print('-'*15,'%d. omega = %.4f'%(i+1,w),'-'*15)
            h[i]=vibErg(5,1,0.4,A,w,2)
        #g[i]=vibErg(6,-2,0.4,A,w,2)
        #l[i]=vibErg(6,0,0.4,A,w)
    plot(olst,h/S,'go-',label='Special Spring at Left')
    #plot(olst,g/S,'bo--',label='Special Spring at Right')
    #plot(olst,l/S,'ro-',label=r'Ordinary')
    ylabel('Resonance')
    xlabel('Frequency')
    grid()
    #title('Extra Friction in Different End')#No title is needed since it should be shown in the document level
    legend()
    savefig('dwell.pdf')
    show()
